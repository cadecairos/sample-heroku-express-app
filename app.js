'use strict';

const express = require('express');

const app = express();
const notFoundHTML = `
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>404 - Page not found</title>
  </head>
  <body>
    <p>404 - Page not found</p>
  </body>
  </html>`;

app.disable('x-powered-by');

app.use(express.static('public'));

app.use(function(req, res, next) {
  res.status(404).send(notFoundHTML);
});

app.listen(process.env.PORT, function() {
  console.log(`Running server at: ${process.env.PORT}`);
});

const shutdown = function() {
  server.close(function() {
    process.exit(0);
  });
};

process.on('SIGINT', shutdown);
process.on('SIGTERM', shutdown);